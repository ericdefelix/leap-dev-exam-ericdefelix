module.exports = function(grunt) {
	grunt.initConfig({
		sass: {                            
			dist: {                       
				options: {
					style: 'compressed'
				},
				files: {                         
					'dist/styles.css': 'src/styles.scss'
				}
			}
		},
		copy: {
			main: {
				files: [
					{
						expand: true,
						flatten: true,
						src: ['src/images/*'], 
						dest: 'dist/images',
					}
				],
			},
		},
		concat: {
		    options: {
		    	separator: ';'
		    },
			dist: {
				files: {
					'dist/scripts.js': [
						'node_modules/vue/dist/vue.min.js', 'node_modules/jquery/dist/jquery.min.js',
						'node_modules/slick-carousel/slick/slick.js','node_modules/velocity-animate/velocity.min.js',
						'node_modules/velocity-animate/velocity.ui.min.js','src/app.js'
					]
				}
			}
		},
		processhtml: {
			dist: {
				files: {
					'dist/index.html': ['src/index.html']
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.registerTask('default', ['sass', 'copy', 'concat','processhtml']);
}