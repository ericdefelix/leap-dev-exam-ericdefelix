let listData = [];
let menuData = [];

for (var i = 6; i >= 0; i--) {
	listData.push({
		iconclass: 'test',
		text: 'The quick brown Fantastic Mr. Fox jumped over the lazy bloke',
		subtext: 'Oops I did it again',
		modifiedby: 'Eric de Felix',
		modifieddate: '21/11/2018',
		status: 'Active'
	})
}

for (var i = 0; i <= 1; i++) {
	menuData.push({ menu_group: [] });

	for (var x = 0; x <= 2; x++) {
		menuData[i]['menu_group'].push({ menu_item: 'Menu Item ' + x });
	}
}

var list = new Vue({
	el: '#listDom',
	data: { list: listData }
})

var menu = new Vue({
	el: '#menuDom',
	data: { menu: menuData }
})

var carousel = new Vue({ el: '#carousel', data: {} });

var slick_carousel = {
	init: function() {
		$('#carousel').slick({
			centerMode: false,
			dots: true,
			speed: 500,
			autoplay: false,
			arrows: false,
			cssEase: 'linear',
			slideToShow: 3,
			mobileFirst: true,
			responsive: [
				{
					breakpoint: 1440,
					settings: { slidesToShow: 4, dots: true }
				},
				{
					breakpoint: 995,
					settings: { dots: true, slidesToShow: 3 }
				},
				{
					breakpoint: 690,
					settings: { slidesToShow: 1 }
				}
			]
		});
	},
	build: function() {
		this.init();
	}
}

var ui = {
	header: $('.header-fixed'),
	sidebar: $('#sidebar'),
	init: function() {
		// ui.adjust_dropdown_adaptive();
		$('<div class="overlay-sidebar"></div>').appendTo($('body'));
	},
	bind: function() {
		$(document).on('click', '[data-toggle="dropdown"]', function(event) {
			
		});

		$(document).on('click', function(event) {
			if ($(event.target).attr('data-toggle') == 'dropdown') {
				if ($(this).find('.dropdown-menu.in').length) {
					$(this).find('.dropdown-menu.in').removeClass('in');
				}
				$(event.target).next('.dropdown-menu').addClass('in');
			}
			else {
				$(this).find('.dropdown-menu.in').removeClass('in');
			}
		});

		$(document).on('click', '[data-toggle="sidebar"]', function(event) {
			ui.toggle_sidebar();
		});

		$(document).on('click', '.overlay-sidebar', function(event) {
			ui.toggle_sidebar();
		});

        var offset = ui.header.position().top;
        $(window).on('scroll', function(event) {
            ui.toggle_fixed_header(offset);
        });

        this.toggle_fixed_header();
	},
	adjust_dropdown_adaptive: function() {
		// $('.dropdown-adaptive').each(function() {
		// 	var minWidth = 0, dropdown_menu = $(this).find('.dropdown-menu');
		// 	dropdown_menu.children('.btn').each(function() {
		// 		$(this).css({ position: 'absolute' })
		// 		minWidth += $(this).outerWidth();
		// 	});

		// 	dropdown_menu.css('min-width',minWidth + 15);
		// });
	},
	toggle_sidebar: function(btn) {
		var body = $('body');

		if (!body.hasClass('body-freeze')) {
			body
				.css('height', $(document).height())
				.addClass('body-freeze')
				.velocity({
					translateX: ui.sidebar.width() 
				},{
					duration: 300,
					easing: 'ease-in-out',
					begin: function() {
						$(document).find('.overlay-sidebar').velocity('transition.fadeIn');
					}
				});
		}
		else {
			body
				.removeClass('body-freeze')
				.velocity({
					translateX: 0 
				},{
					duration: 300,
					easing: 'ease-in-out',
					begin: function() {
						$(document).find('.overlay-sidebar').velocity('transition.fadeOut');
					},
					complete: function() {
						$(this)
							.css({ height: 'auto', transform: 'none' });
					}
				});
		}
	},
    toggle_fixed_header: function(offset) {
        var win_scroll = $(window).scrollTop(),
            parent = $(document).find('[data-sticky-adjust]');

        parent.css('padding-top', ui.header.height() + 4);
        if (win_scroll >= 1) {
            ui.header.addClass('in');
        }
        else {
            ui.header.removeClass('in');
        }
    },
	build: function() {
		this.init();
		this.bind();
		slick_carousel.build();
	}
}

$(document).ready(function() {
	ui.build();	
});